package controller;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

public class UserController {

	private IUserPersistance userPersistance;

	public UserController(IPersistance persistance) {
		this.userPersistance = persistance.getUserPersistance();
	}

	/**
	 * erstellt einen User in der Datenbank
	 * 
	 * @param user
	 *            | nutzer der erstellt wird
	 */
	public void createUser(User user) {
		if (this.userPersistance.readUser(user.getLoginname()) == null) {
			this.userPersistance.createUser(user);
		}
	}

	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * 
	 * @param username
	 *            eindeutige Loginname
	 * @param passwort
	 *            das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return userPersistance.readUser(username);
	}

	/**
	 * ver�ndert einen User in der Datenbank
	 * 
	 * @param user
	 *            | nutzer der ver�ndert wird
	 */
	public void changeUser(User user) {
		if (this.userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.updateUser(user);
		}
	}

	/**
	 * l�scht einen User falls vorhanden
	 * 
	 * @param user
	 *            | nutzer der entfernt wird
	 */
	public void deleteUser(User user) {
		if (this.userPersistance.readUser(user.getLoginname()) != null) {
			this.userPersistance.deleteUser(user);
		}
	}

	/**
	 * schaut ob beide passw�rter �bereinstimmen
	 * 
	 * @param username
	 *            eindeutige Loginname
	 * @param passwort
	 *            das richtige Passwort
	 * @return
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username, passwort);
		if (user.getPassword().equals(passwort)) {
			return true;
		}
		return false;
	}
}
