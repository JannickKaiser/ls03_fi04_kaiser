package view.menue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.UserController;
import model.User;



public class CreateUserWindow extends JFrame{
	private UserController controller;
	private JTextField textFieldVor;
	private JTextField textFieldNach;
	private JTextField textFieldGeb;
	private JTextField textFieldStr;
	private JTextField textFieldHausnum;
	private JTextField textFieldPostleit;
	private JTextField textFieldStadt;
	private JTextField textFieldLogin;
	private JTextField textFieldPass;
	private JTextField textFieldGehalt;
	private JTextField textFieldFamilienstand;
	private JTextField textFieldAbschlussnote;
	
	public CreateUserWindow(UserController controller) {
        this.controller = controller;
		getContentPane().setLayout(null);
		setSize(400,700);
		
		JLabel lblVorname = new JLabel("Vorname:");
		lblVorname.setBounds(10, 21, 139, 14);
		getContentPane().add(lblVorname);
		
		JLabel lblNachname = new JLabel("Nachname:");
		lblNachname.setBounds(10, 46, 139, 14);
		getContentPane().add(lblNachname);
		
		JLabel lblGeb = new JLabel("Geburtstag:");
		lblGeb.setBounds(10, 71, 139, 14);
		getContentPane().add(lblGeb);
		
		JLabel lblStr = new JLabel("Stra\u00DFe:");
		lblStr.setBounds(10, 96, 139, 14);
		getContentPane().add(lblStr);
		
		JLabel lblHausnummer = new JLabel("Hausnummer:");
		lblHausnummer.setBounds(10, 121, 139, 14);
		getContentPane().add(lblHausnummer);
		
		JLabel lblPostleitzahl = new JLabel("Postleitzahl:");
		lblPostleitzahl.setBounds(10, 146, 139, 14);
		getContentPane().add(lblPostleitzahl);
		
		JLabel lblStadt = new JLabel("Stadt:");
		lblStadt.setBounds(10, 171, 139, 14);
		getContentPane().add(lblStadt);
		
		JLabel lblLogin = new JLabel("Login Name:");
		lblLogin.setBounds(10, 196, 139, 14);
		getContentPane().add(lblLogin);
		
		JLabel lblPasswort = new JLabel("Passwort:");
		lblPasswort.setBounds(10, 221, 139, 14);
		getContentPane().add(lblPasswort);
		
		JLabel lblGehalt = new JLabel("Gehalt:");
		lblGehalt.setBounds(10, 246, 139, 14);
		getContentPane().add(lblGehalt);
		
		JLabel lblFam = new JLabel("Familienstand:");
		lblFam.setBounds(10, 271, 139, 14);
		getContentPane().add(lblFam);
		
		JLabel lblAbschlussnote = new JLabel("Abschlussnote:");
		lblAbschlussnote.setBounds(10, 296, 139, 14);
		getContentPane().add(lblAbschlussnote);
		
		textFieldVor = new JTextField();
		textFieldVor.setBounds(159, 18, 86, 20);
		getContentPane().add(textFieldVor);
		textFieldVor.setColumns(10);
		
		textFieldNach = new JTextField();
		textFieldNach.setBounds(159, 43, 86, 20);
		getContentPane().add(textFieldNach);
		textFieldNach.setColumns(10);
		
		textFieldGeb = new JTextField();
		textFieldGeb.setBounds(159, 68, 86, 20);
		getContentPane().add(textFieldGeb);
		textFieldGeb.setColumns(10);
		
		textFieldStr = new JTextField();
		textFieldStr.setBounds(159, 93, 86, 20);
		getContentPane().add(textFieldStr);
		textFieldStr.setColumns(10);
		
		textFieldHausnum = new JTextField();
		textFieldHausnum.setBounds(159, 118, 86, 20);
		getContentPane().add(textFieldHausnum);
		textFieldHausnum.setColumns(10);
		
		textFieldPostleit = new JTextField();
		textFieldPostleit.setBounds(159, 143, 86, 20);
		getContentPane().add(textFieldPostleit);
		textFieldPostleit.setColumns(10);
		
		textFieldStadt = new JTextField();
		textFieldStadt.setBounds(159, 168, 86, 20);
		getContentPane().add(textFieldStadt);
		textFieldStadt.setColumns(10);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setBounds(159, 193, 86, 20);
		getContentPane().add(textFieldLogin);
		textFieldLogin.setColumns(10);
		
		textFieldPass = new JTextField();
		textFieldPass.setBounds(159, 218, 86, 20);
		getContentPane().add(textFieldPass);
		textFieldPass.setColumns(10);
		
		textFieldGehalt = new JTextField();
		textFieldGehalt.setBounds(159, 243, 86, 20);
		getContentPane().add(textFieldGehalt);
		textFieldGehalt.setColumns(10);
		
		textFieldFamilienstand = new JTextField();
		textFieldFamilienstand.setBounds(159, 268, 86, 20);
		getContentPane().add(textFieldFamilienstand);
		textFieldFamilienstand.setColumns(10);
		
		textFieldAbschlussnote = new JTextField();
		textFieldAbschlussnote.setBounds(159, 293, 86, 20);
		getContentPane().add(textFieldAbschlussnote);
		textFieldAbschlussnote.setColumns(10);
		
		JButton btnNewButton = new JButton("Erstelle Nutzer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		            LocalDate localDate = LocalDate.parse(textFieldGeb.getText(), formatter);
		            
		            User user = new User(
		                    -1, 
		                    textFieldVor.getText(), 
		                    textFieldNach.getText(), 
		                    localDate,  
		                    textFieldStr.getText(), 
		                    textFieldHausnum.getText(), 
		                    textFieldPostleit.getText(), 
		                    textFieldStadt.getText(), 
		                    textFieldLogin.getText(), 
		                    textFieldPass.getText(),
		                    Integer.parseInt(textFieldGehalt.getText()), 
		                    textFieldFamilienstand.getText(), 
		                    Integer.parseInt(textFieldAbschlussnote.getText())
		            );
		            controller.createUser(user);
		            System.out.println("Der Benutzer " + user.getLoginname() + " wurde erstellt.");
			}
		});
		btnNewButton.setBounds(10, 321, 235, 32);
		getContentPane().add(btnNewButton);
		
		JLabel lblLegenSieEinen = new JLabel("Legen Sie einen neuen Nutzer an");
		lblLegenSieEinen.setHorizontalAlignment(SwingConstants.CENTER);
		lblLegenSieEinen.setBounds(10, 0, 235, 14);
		getContentPane().add(lblLegenSieEinen);
		

		
		
	}
	
}
